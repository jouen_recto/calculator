var parent_category = ['Warehouse','Transportation','Other'];
var warehouse = ['By the bottle','By the case','by the pallet','Other'];
var warehouse_by_the_bottle = ['Storage','Handling'];
var warehouse_by_the_bottle_storage = ['Dry func','Refrigerated func'];
var warehouse_by_the_bottle_handling = ['Bottle pick func','Bottle labeling func'];
var warehouse_by_the_case = ['Storage','Handling'];
var warehouse_by_the_case_storage = ['Dry func','Refrigerated func','Climate Controlled func'];
var warehouse_by_the_case_handling = ['Slipsheet unload container','Unload floor loaded container','Destroy product','Labeling','Ship FedEx/UPS'];
var warehouse_by_the_pallet = ['Storage func','Handling func'];
var transportation = ['PLCB','Other'];
var transportation_plcb = ['Trucking','SLO'];
var transportation_plcb_trucking = ['NJ','NYC','Baltimore','DC','Philadelphia'];

function gen_slug(str)
{
	var val = str.toLowerCase();
	val = val.replace(/[^a-zA-Z ]/g, "");
	val = val.replace(/ /g,'_');
	
	return val;

}

function check_if_function(str){
	if (str.toLowerCase().indexOf("func") >= 0)
	{
		return true;
	}else
	{
		return false;
	}
}

function check_name(str)
{
	val = str.replace(' func', "");
	
	return val;
}

function contactFormClose()
{
	jQuery(".barry_modal").removeClass('lm-show-modal');
	jQuery("body").removeClass('lm-no-overflow');
}

//functions
function warehouse_by_the_bottle_storage_dry_func()
{
	jQuery(".function-body").empty();

	var div_parent = jQuery(".function-body");
	div_parent.append("<label>Number of bottles</label>");
	div_parent.append('<input type="text" id="txt-dry-num" name="txt-dry-num" >');

	var btn = jQuery('<button class="lm-btn lm-btn-primary" >Compute</button>');
	btn.click(function(){
		var num = parseInt(jQuery("#txt-dry-num").val());

		var result = num * 0.01;

		jQuery(".dry-result").html('<p>Your estimated price is $'+result+' per day.  Handling in is $.25/btl, handling out is $.25/btl.</p>').show();
		console.log(num);
	});
	div_parent.append(btn);
	div_parent.append('<div class="dry-result result" style="display:none;"></div>');


}

function warehouse_by_the_bottle_storage_refrigerated_func()
{
	jQuery(".function-body").empty();

	var div_parent = jQuery(".function-body");
	div_parent.append("<label>Number of bottles</label>");
	div_parent.append('<input type="text" id="txt-ref-num" name="txt-ref-num" >');

	var btn = jQuery('<button class="lm-btn lm-btn-primary" >Compute</button>');
	btn.click(function(){
		var num = parseInt(jQuery("#txt-ref-num").val());

		var result = num * 0.015;

		jQuery(".ref-result").html('<p>Your estimated price is $'+result+' per day.  Handling in is $.25/btl, handling out is $.25/btl.</p>').show();
		console.log(num);
	});
	div_parent.append(btn);
	div_parent.append('<div class="ref-result result" style="display:none;"></div>');


}

function warehouse_by_the_bottle_handling_bottle_pick_func()
{
	jQuery(".function-body").empty();

	var div_parent = jQuery(".function-body");
	div_parent.append("<label>Number of bottles</label>");
	div_parent.append('<input type="text" id="txt-pick-num" name="txt-pick-num" >');

	var btn = jQuery('<button class="lm-btn lm-btn-primary" >Compute</button>');
	btn.click(function(){
		var num = parseInt(jQuery("#txt-pick-num").val());

		var result = num * 0.52;

		jQuery(".pick-result").html('<p>Your estimated price is $'+result+'.</p>').show();
		console.log(num);
	});
	div_parent.append(btn);
	div_parent.append('<div class="pick-result result" style="display:none;"></div>');
}

function warehouse_by_the_bottle_handling_bottle_labeling_func()
{
	jQuery(".function-body").empty();

	var div_parent = jQuery(".function-body");
	div_parent.append("<label>Number of bottles</label>");
	div_parent.append('<input type="text" id="txt-labeling-num" name="txt-labeling-num" >');

	var btn = jQuery('<button class="lm-btn lm-btn-primary" >Compute</button>');
	btn.click(function(){
		var num = parseInt(jQuery("#txt-labeling-num").val());

		var result = num * 0.77;

		jQuery(".labeling-result").html('<p>Your estimated price is $'+result+'.</p>').show();
		console.log(num);
	});
	div_parent.append(btn);
	div_parent.append('<div class="labeling-result result" style="display:none;"></div>');
}

function warehouse_by_the_case_storage_dry_func()
{
	jQuery(".function-body").empty();

	var div_parent = jQuery(".function-body");
	div_parent.append("<label>Number of bottles</label>");
	div_parent.append('<input type="text" id="txt-dry-num" name="txt-dry-num" >');

	var btn = jQuery('<button class="lm-btn lm-btn-primary" >Compute</button>');
	btn.click(function(){
		var num = parseInt(jQuery("#txt-dry-num").val());

		var result = num * 0.014;

		jQuery(".dry-result").html('<p>Your estimated price is $'+result+' per day.  Handling in is $.32/cs, handling out is $.32/cs.</p>').show();
		console.log(num);
	});
	div_parent.append(btn);
	div_parent.append('<div class="dry-result result" style="display:none;"></div>');
}

function warehouse_by_the_case_storage_refrigerated_func()
{
	jQuery(".function-body").empty();

	var div_parent = jQuery(".function-body");
	div_parent.append("<label>Number of bottles</label>");
	div_parent.append('<input type="text" id="txt-refrigerated-num" name="txt-refrigerated-num" >');

	var btn = jQuery('<button class="lm-btn lm-btn-primary" >Compute</button>');
	btn.click(function(){
		var num = parseInt(jQuery("#txt-refrigerated-num").val());

		var result = num * 0.018;

		jQuery(".refrigerated-result").html('<p>Your estimated price is $'+result+' per day.  Handling in is $.42/cs, handling out is $.42/cs.</p>').show();
		console.log(num);
	});
	div_parent.append(btn);
	div_parent.append('<div class="refrigerated-result result" style="display:none;"></div>');
}

function warehouse_by_the_case_storage_climate_controlled_func()
{
	jQuery(".function-body").empty();

	var div_parent = jQuery(".function-body");
	div_parent.append("<label>Number of bottles</label>");
	div_parent.append('<input type="text" id="txt-climate-num" name="txt-climate-num" >');

	var btn = jQuery('<button class="lm-btn lm-btn-primary" >Compute</button>');
	btn.click(function(){
		var num = parseInt(jQuery("#txt-climate-num").val());

		var result = num * 0.026;

		jQuery(".climate-result").html('<p>Your estimated price is $'+result+' per day.  Handling in is $.52/cs, handling out is $.52/cs.</p>').show();
		console.log(num);
	});
	div_parent.append(btn);
	div_parent.append('<div class="climate-result result" style="display:none;"></div>');
}


function warehouse_by_the_pallet_storage_func()
{
	jQuery(".function-body").empty();
	var div_parent = jQuery(".function-body");
	var msg = 'Storage $.27/pallet/day, in/out is $4.16/pallet.';
	div_parent.append('<div class="climate-result result">'+msg+'</div>');
}

function warehouse_by_the_pallet_handling_func()
{
	jQuery(".function-body").empty();
	var div_parent = jQuery(".function-body");
	var msg = '$9.50/pallet.';
	div_parent.append('<div class="climate-result result">'+msg+'</div>');
}

jQuery(document).ready(function(){

	//set parent options
	jQuery("#lm-sel-parent").append('<option value="default">-- Select service --</option>');
	parent_category.forEach(function(val){
		
		jQuery("#lm-sel-parent").append('<option value="'+gen_slug(val)+'">'+check_name(val)+'</option>');

	});

	//function parent options
	jQuery("#lm-sel-parent").change(function(){
		var option = jQuery(this).val();

		//if option equal other show the modal for barry miller
		if(option == "other")
		{
			jQuery(".barry_modal").addClass('lm-show-modal');
			jQuery("body").addClass('lm-no-overflow');
		}else if(option == "default")
		{
			jQuery(".function-body").empty();
			jQuery("#lm-sel-third-con").hide();
			jQuery("#lm-sel-second-con").hide();
			jQuery("#lm-sel-four-con").hide();
		}else if(check_if_function(option))
		{
			window[option]();
		}else
		{
			jQuery(".function-body").empty();
			jQuery("#lm-sel-third-con").hide();
			jQuery("#lm-sel-four-con").hide();
			jQuery("#lm-sel-second-con").show();
			jQuery("#lm-sel-second").empty();
			jQuery("#lm-sel-second-label").html(jQuery(this).find(":selected").text());
			jQuery("#lm-sel-second").append('<option value="default">-- Select --</option>');
			window[option].forEach(function(val){
				
				jQuery("#lm-sel-second").append('<option value="'+gen_slug(val)+'">'+check_name(val)+'</option>');

			});
		}
	});

	jQuery("#lm-sel-second").change(function(){
		var parent = jQuery('#lm-sel-parent').val();
		var option = jQuery(this).val();

		//if option equal other show the modal for barry miller
		if(option == "other")
		{
			jQuery("#lm-sel-third-con").hide();
			jQuery("#lm-sel-four").hide();
			jQuery(".barry_modal").addClass('lm-show-modal');
			jQuery("body").addClass('lm-no-overflow');
		}else if(option == "default")
		{
			jQuery(".function-body").empty();
			jQuery("#lm-sel-third-con").hide();
			jQuery("#lm-sel-four").hide();
		}else if(check_if_function(option))
		{
			window[parent+"_"+option]();
		}else
		{
			jQuery(".function-body").empty();
			jQuery("#lm-sel-four-con").hide();
			jQuery("#lm-sel-third-con").show();
			jQuery("#lm-sel-third").empty();
			jQuery("#lm-sel-third-label").html(jQuery(this).find(":selected").text());
			jQuery("#lm-sel-third").append('<option value="default">-- Select --</option>');
			window[parent+'_'+option].forEach(function(val){
				
				jQuery("#lm-sel-third").append('<option value="'+gen_slug(val)+'">'+check_name(val)+'</option>');

			});
		}
	});

	jQuery("#lm-sel-third").change(function(){
		var parent = jQuery('#lm-sel-parent').val();
		var parent_sec = jQuery('#lm-sel-second').val();
		var option = jQuery(this).val();

		//if option equal other show the modal for barry miller
		if(option == "other")
		{
			
			jQuery("#lm-sel-four-con").hide();
			jQuery(".barry_modal").addClass('lm-show-modal');
			jQuery("body").addClass('lm-no-overflow');
		}else if(option == "default")
		{
			jQuery(".function-body").empty();
			jQuery("#lm-sel-four-con").hide();
		}else if(check_if_function(option))
		{
			window[parent+"_"+parent_sec+"_"+option]();
		}else
		{
			jQuery(".function-body").empty();
			jQuery("#lm-sel-four-con").show();
			jQuery("#lm-sel-four").empty();
			jQuery("#lm-sel-four-label").html(jQuery(this).find(":selected").text());
			jQuery("#lm-sel-four").append('<option value="default">-- Select --</option>');
			window[parent+'_'+parent_sec+'_'+option].forEach(function(val){	
				jQuery("#lm-sel-four").append('<option value="'+gen_slug(val)+'">'+check_name(val)+'</option>');

			});
		}
	});

	jQuery("#lm-sel-four").change(function(){
		var parent = jQuery('#lm-sel-parent').val();
		var parent_sec = jQuery('#lm-sel-second').val();
		var parent_third = jQuery('#lm-sel-third').val();
		var option = jQuery(this).val();
		console.log(option);
		//if option equal other show the modal for barry miller
		if(option == "other")
		{
			
			//jQuery("#lm-sel-four-con").hide();
			jQuery(".barry_modal").addClass('lm-show-modal');
			jQuery("body").addClass('lm-no-overflow');
		}else if(option == "default")
		{
			//jQuery("#lm-sel-four-con").hide();
		}else if(check_if_function(option))
		{
			window[parent+"_"+parent_sec+"_"+parent_third+"_"+option]();
		}else
		{
			
			// jQuery("#lm-sel-four-con").show();
			// jQuery("#lm-sel-four").empty();
			// jQuery("#lm-sel-four-label").html(jQuery(this).find(":selected").text());
			// jQuery("#lm-sel-four").append('<option value="default">-- Select --</option>');
			// window[parent+'_'+parent_sec+'_'+option].forEach(function(val){
			// 	val = check_name(val);
			// 	jQuery("#lm-sel-four").append('<option value="'+gen_slug(val)+'">'+val+'</option>');

			// });
		}
	});
});