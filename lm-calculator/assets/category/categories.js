var parent_category = ['Warehouse','Transportation'];
var warehouse = ['By the bottle','By the case','by the pallet'];
var warehouse_by_the_bottle = ['Storage','Handling'];
var by_the_bottle_storage = ['Dry','Refrigerated'];
var by_the_bottle_handling = ['Bottle pick','Bottle labeling'];
var warehouse_by_the_case = ['Storage','Handling'];
var by_the_case_storage = ['Dry','Refrigerated','Climate Controlled'];
var by_the_case_handling = ['Slipsheet unload container','Unload floor loaded container','Destroy product','Labeling','Ship FedEx/UPS'];
var warehouse_by_the_pallet = ['Storage','Handling'];
var transportation = ['PLCB'];
var transportation_plcb = ['Trucking','SLO'];
var plcb_trucking = ['NJ','NYC','Baltimore','DC','Philadelphia'];

