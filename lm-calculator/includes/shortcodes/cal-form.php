<?php

function cal_form_init()
{

	include "views/form.php";
	wp_enqueue_style('lm-cal-form-css', PLUGIN_URL .'assets/css/lm-calculator.css');
	wp_enqueue_script('lm-cal-form-js', PLUGIN_URL .'assets/js/lm-calculator.js', array('jquery'), null, true);
}

add_shortcode('cal_form', 'cal_form_init');
?>