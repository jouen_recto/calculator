<?php

function load_contact_modal_init($args){
   $modal = '<div class="lm-modal-contact '.$args[2].'" >';
   	$modal .= '<div class="lm-modal-header"><h3>'.$args[0].'</h3></div>';
   	$modal .= '<div class="lm-modal-body" data-view="'.$args[1].'">';
   	$modal .= '</div>';
   	$modal .= '<div class="lm-modal-footer">';
   	$modal .= '<button class="lm-btn lm-btn-default" >Send</button>';
   	$modal .= '<button class="lm-btn lm-btn-danger" onClick="contactFormClose()" >Close</button>';
   	$modal .= '</div>';
   $modal .= '</div>';
   $script = "<script>  jQuery('body').append('".$modal."'); </script>";
   
   echo $script;
}

add_action( 'load_contact_modal', 'load_contact_modal_init' );


?>